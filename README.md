# Kubernetes single-master cluster on Ubuntu Cloud
This repository consists in an ansible playbook to do the most simple Kubernetes deployment.  


## ! WARNING !  
This is intended for a testing cluster, there are no verifications of any sort.  
Re-running the playbook will reset the k8s cluster.  
Security might be overlooked for convenience.


## Contents:
Example playbook and inventory.  
K8s dashboard deployment files.  
Playbook to fetch credentials to the ansible runner machine.


## How to use:
* The use of [cloud-init][cloud-init] is suggested, create an **ansible user** with a **ssh key** on every machine.
* Use your hypervisor of choice or maybe the cloud and deploy at least **1 kube_master** and **1 kube_node**.
* Populate your ansible [inventory][hosts-ini] with hostnames and IPs of the machines.

```ini
[all]
kube-master-001 ansible_host=10.1.0.10
kube-node-001   ansible_host=10.1.0.11
kube-node-002   ansible_host=10.1.0.12

[kube_master]
kube-master-001

[kube_node]
kube-node-001
kube-node-002

[k8s-cluster:children]
kube_master
kube_node

[k8s_cluster:vars]
ansible_user=ansible
ansible_become=yes
ansible_become_method=sudo
ansible_ssh_private_key_file="~/.ssh/AnsibleNode"
```

* Run:
```bash
ansible-playbook site.yml
```

* Run on `kube_master[0]`:
```bash
$ kubectl get nodes
NAME                 STATUS   ROLES    AGE     VERSION
kube-master-mruivr   Ready    master   5m30s   v1.13.4
kube-node-msuyna     Ready    <none>   4m50s   v1.13.4
kube-node-pphkcc     Ready    <none>   4m50s   v1.13.4

$ kubectl get pods --all-namespaces
NAMESPACE     NAME                                         READY   STATUS    RESTARTS   AGE
kube-system   coredns-86c58d9df4-bdfvd                     1/1     Running   0          5m13s
kube-system   coredns-86c58d9df4-lbjdk                     1/1     Running   0          5m13s
kube-system   etcd-kube-master-mruivr                      1/1     Running   0          4m36s
kube-system   kube-apiserver-kube-master-mruivr            1/1     Running   0          4m30s
kube-system   kube-controller-manager-kube-master-mruivr   1/1     Running   0          4m34s
kube-system   kube-flannel-ds-amd64-j72sr                  1/1     Running   0          4m52s
kube-system   kube-flannel-ds-amd64-q8v98                  1/1     Running   0          5m13s
kube-system   kube-flannel-ds-amd64-qrtft                  1/1     Running   0          4m52s
kube-system   kube-proxy-59thf                             1/1     Running   0          5m13s
kube-system   kube-proxy-p64gf                             1/1     Running   0          4m52s
kube-system   kube-proxy-s6mr5                             1/1     Running   0          4m52s
kube-system   kube-scheduler-kube-master-mruivr            1/1     Running   0          4m6s
```

## Extras:
### To use the kubernetes dashboard:
* After running the whole playbook there should be a `tokens` directory, otherwise run:
```bash
ansible-playbook site.yml --tags 'tokens'
```
* The `tokens` directory should have two files (`config` and `dashboard-token.txt`).
* Copy the `config` file into the `~/.kube` directory on your local machine.
* Run `kubectl proxy` from your local machine.
* The dashboard will be available at [> this link <][dashboard-api], open it on a new browser tab.
* Select `Token` and paste the contents from `dashboard-token.txt` on `Enter token`.
* Finally click `SIGN IN`.

(instructions taken from the [kubernetes dashboard documentation][dashboard-docs])

### To just install packages and OS tweaks on machines:
```bash
ansible-playbook site.yml --tags 'bootstrap'
```

### Reset cluster (without bootstrapping again):
```bash
ansible-playbook site.yml --tags 'master,nodes'
```

### To add a new `kube-node-`, run:
```bash
ansible-playbook site.yml --tags 'nodes,bootstrap-node' --limit 'kube_master[0], kube-node-003, kube-node-004'
```
(^ include `kube_master[0]` to create a join token, otherwise it fails ^)


## Resources:
This is based on the official [kubeadm documentation][kubeadm-docs].  
The [Ubuntu Cloud 18.04 LTS][ubuntu-cloud] image was used.

[dashboard-docs]: https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/
[dashboard-api]: http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/
[kubeadm-docs]: https://kubernetes.io/docs/setup/independent/create-cluster-kubeadm/
[ubuntu-cloud]: https://cloud-images.ubuntu.com/bionic/current/bionic-server-cloudimg-amd64.img
[cloud-init]: https://cloudinit.readthedocs.io/en/latest/
[hosts-ini]: ansible/hosts.ini

